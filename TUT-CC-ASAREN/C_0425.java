import java.util.*;

class Main{
	
	Scanner sc;
	
	int MAX = 1000000;
	void run() {
		int[] t = new int[200];
		int tn = 0;
		for(int i = 0;i<200;i++){
			t[i] = (int)i * (i + 1) * (i + 2) / 6;
			if(t[i] % 2 != 0){
				tn++;
			}
		}
		int[] to = new int[tn];
		int ti = 0;
		for(int i=0;i<200;i++){
			if(t[i] % 2 != 0){
				to[ti] = t[i];
				ti++;
			}
		}
		int[] dp = new int[MAX+1];
		for(int i=0;i<=MAX;i++)dp[i] = Integer.MAX_VALUE;
		
		dp[0] = 0;
		for(int j=1;j<200;j++){
			for(int i=1;i<=MAX;i++){
				if(i - t[j] >= 0){
					dp[i] = Math.min(dp[i], dp[i-t[j]] + 1);
				}
			}
		}
		int[] dpo = new int[MAX+1];
		for(int i=0;i<=MAX;i++)dpo[i] = Integer.MAX_VALUE;

		dpo[0] = 0;
		for(int j=0;j<tn;j++){
			for(int i=1;i<=MAX;i++){
				if(i - to[j] >= 0){
					dpo[i] = Math.min(dpo[i], dpo[i-to[j]] + 1);
				}
			}
		}
		
		int n;
		while(true){
			n = ni();
			if(n==0)break;
			
			System.out.println(dp[n] + " " + dpo[n]);
		}
	}
	
	Main () {
		sc = new Scanner(System.in);
	}
	
	int ni() {
		return sc.nextInt();
	}
	
	public static void main(String[] args){
		new Main().run();
	}
	
	
	
	void debug(Object... os){
		System.out.println(Arrays.deepToString(os));
	}
}
import java.util.*;
import java.math.*;

class Main{
	
	Scanner sc;
	char[][] m;
	StringBuilder tmpsb;
	
	void run() {
		int w, h;
		while(true){
			w = ni();
			h = ni();
			if((w|h) == 0)break;
			m = new char[h][w];
			for(int i=0;i<h;i++){
				String line = sc.next();
				for(int j=0;j<w;j++){
					char c = line.charAt(j);
					if('0'<= c && c <= '9'){
						m[i][j] = c;
					}else{
						m[i][j] = '-';
					}
				}
			}
//			debug(m);
			StringBuilder[][] dp = new StringBuilder[h][w];
			StringBuilder max = new StringBuilder("0");
			for(int i=h-1;i>=0;i--){
				for(int j=w-1;j>=0;j--){
					if(m[i][j] == '-'){
						dp[i][j] = new StringBuilder("");
						continue;
					}
					if(i == h-1 && j == w-1){
						dp[i][j] = new StringBuilder("").append(m[i][j]);
					}else if(i == h-1){
						dp[i][j] = new StringBuilder("").append(m[i][j]).append(dp[i][j+1]);
					}else if(j == w-1){
						dp[i][j] = new StringBuilder("").append(m[i][j]).append(dp[i+1][j]);
					}else{
						dp[i][j] = max(
							new StringBuilder("").append(m[i][j]).append(dp[i+1][j]), 
							new StringBuilder("").append(m[i][j]).append(dp[i][j+1])
						);
					}
					max = max(dp[i][j], max);
				}
			}
			System.out.println(max);
		}
	}
	
	StringBuilder max(StringBuilder sb1, StringBuilder sb2){
//		debug(sb1, sb2);
		BigInteger b1 = new BigInteger(sb1.toString());
		BigInteger b2 = new BigInteger(sb2.toString());
		return (b1.compareTo(b2) > 0) ? sb1 : sb2;
	}
	
	int ni() {
		return sc.nextInt();
	}
	
	Main(){
		sc = new Scanner(System.in);
	}
	
	public static void main(String[] args){
		new Main().run();
	}
	
	void debug(Object... os){
		System.out.println(Arrays.deepToString(os));
	}
}
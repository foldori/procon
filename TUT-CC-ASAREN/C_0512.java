import java.io.*;
import java.util.*;

class Main{
	public void run(){
		int n;
		while(true){
			n = ni();
			if(n == 1)break;
			
			boolean[] msPrime = new boolean[300000+1];
			for(int i=0;i<=300000;i++)msPrime[i] = true;
			msPrime[0] = false;
			for(int i=1;i<=300000;i++){
				if(i%7==1 || i%7==6){
//					msPrime[i] = true;
					if(i == 1)continue;
					
					for(int j=i*2;j<=300000;j+=i){
						msPrime[j] = false;
					}
					
				}else{
					msPrime[i] = false;
				}
			}
			out.printf("%d:", n);
			for(int i=6;i<=300000;i++){
				if(msPrime[i] && n % i == 0/* && msPrime[n / i]*/)out.printf(" %d", i);
			}
			out.println();
		}
	}

	private static PrintWriter out;
	public static void main(String[] args){
		out = new PrintWriter(System.out);
		new Main().run();
		out.flush();
	}

	public Main(){
	}

	int ni(){
		int num = 0;
		String str = next();
		boolean minus = false;
		int i = 0;
		if(str.charAt(0) == '-'){
			minus = true;
			i++;
		}
		int len = str.length();
		for(;i < len; i++){
			char c = str.charAt(i);
			if(!('0' <= c && c <= '9')) throw new RuntimeException();
			num = num * 10 + (c - '0');
		}
		return minus ? -num : num;	}

	long nl(){
		long num = 0;
		String str = next();
		boolean minus = false;
		int i = 0;
		if(str.charAt(0) == '-'){
			minus = true;
			i++;
		}
		int len = str.length();
		for(;i < len; i++){
			char c = str.charAt(i);
			if(!('0' <= c && c <= '9')) throw new RuntimeException();
			num = num * 10l + (c - '0');
		}
		return minus ? -num : num;
	}

	String next(){
		int c;
		while(!isAlNum(c = read()));
		StringBuilder build = new StringBuilder();
		build.append((char)c);
		while(isAlNum(c = read())){
			build.append((char)c);
		}
		return build.toString();
	}
	private static byte[] inputBuffer = new byte[1024];
	private static int bufferLength = 0;
	private static int bufferIndex = 0;
	
	private static int read(){
		if(bufferLength < 0) throw new RuntimeException();
		if(bufferIndex >= bufferLength){
			try{
				bufferLength = System.in.read(inputBuffer);
				bufferIndex = 0;
			}catch(IOException e){
				throw new RuntimeException(e);
			}
			if(bufferLength <= 0) return (bufferLength = -1);
		}
		return inputBuffer[bufferIndex++];
	}
	
	private static boolean isAlNum(int c){
		return '!' <= c && c <= '~';
	}
	void debug(Object... os){
		out.println(Arrays.deepToString(os));
	}
}
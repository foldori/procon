import java.io.*;
import java.util.*;

class Main{
	public void run(){
		int[][] rlist = {
		//f= 1  2  3  4  5  6
			{0, 3, 5, 4, 2, 0},//t == 1
			{4, 0, 1, 6, 0, 3},//t == 2
			{2, 6, 0, 0, 1, 5},//t == 3
			{5, 1, 0, 0, 6, 2},//t == 4
			{3, 0, 6, 1, 0, 4},//t == 5
			{0, 4, 2, 5, 3, 0},//t == 6
		};
		
		int[] tx = {0, 1, 0, -1};
		int[] ty = {1, 0, -1, 0};
		
		char[] dirs = {'b', 'r', 'f', 'l'};
		
		int n;
		
		int size = 11;
		int bias = size / 2;
		while(true){
			n=ni();
			if(n==0)break;
			
			int[][] height = new int[size][size];
			int[][] top = new int[size][size];
			
			for(int i=0;i<n;i++){
				int u = ni();
				int f = ni();
				int d = 7 - u;
				int b = 7 - f;
				int r = rlist[f-1][u-1];
				int l = 7 - r;
				
				Dice dice = new Dice(l, r, b, f, d, u);
				int x = bias;
				int y = bias;
				while(true){
					int rollDir = -1;
					int maxNum = -1;
					for(int t=0;t<4;t++){
						int tmpNum = dice.get(dirs[t]);
						if(height[x][y] > height[x + tx[t]][y + ty[t]] && 
							tmpNum >= 4){
							if(maxNum < tmpNum){
								maxNum = tmpNum;
								rollDir = t;
							}
						}
					}
					
					if(rollDir != -1){
						x += tx[rollDir];
						y += ty[rollDir];
						//'b', 'r', 'f', 'l'
						if(rollDir == 0){
							dice.rot(0, 1);
						}else if(rollDir == 1){
							dice.rot(1, 1);
						}else if(rollDir == 2){
							dice.rot(0, -1);
						}else if(rollDir == 3){
							dice.rot(1, -1);
						}
					}else{
						height[x][y]++;
						top[x][y] = dice.get('u');
						break;
					}
				}
			}
			
			int[] res = new int[6];
			for(int i=0;i<size;i++){
				for(int j=0;j<size;j++){
//					out.print(top[i][j] == 0 ? ' ' : (char)(top[i][j]+'0'));
					if(top[i][j] != 0){
						res[top[i][j]-1]++;
					}
				}
//				out.println();
			}
			/*
			for(int i=0;i<size;i++){
				for(int j=0;j<size;j++){
					out.print(height[i][j] == 0 ? ' ' : (char)(height[i][j]+'0'));
				}
				out.println();
			}
			*/
			out.printf("%d %d %d %d %d %d\n", res[0], res[1], res[2], res[3], res[4], res[5]);
		}
	}

	private static PrintWriter out;
	public static void main(String[] args){
		out = new PrintWriter(System.out);
		new Main().run();
		out.flush();
	}

	public Main(){
	}

	int ni(){
		int num = 0;
		String str = next();
		boolean minus = false;
		int i = 0;
		if(str.charAt(0) == '-'){
			minus = true;
			i++;
		}
		int len = str.length();
		for(;i < len; i++){
			char c = str.charAt(i);
			if(!('0' <= c && c <= '9')) throw new RuntimeException();
			num = num * 10 + (c - '0');
		}
		return minus ? -num : num;	}

	long nl(){
		long num = 0;
		String str = next();
		boolean minus = false;
		int i = 0;
		if(str.charAt(0) == '-'){
			minus = true;
			i++;
		}
		int len = str.length();
		for(;i < len; i++){
			char c = str.charAt(i);
			if(!('0' <= c && c <= '9')) throw new RuntimeException();
			num = num * 10l + (c - '0');
		}
		return minus ? -num : num;
	}

	double nd(){
		return Double.parseDouble(next());
	}

	String next(){
		int c;
		while(!isAlNum(c = read()));
		StringBuilder build = new StringBuilder();
		build.append((char)c);
		while(isAlNum(c = read())){
			build.append((char)c);
		}
		return build.toString();
	}
	String nextLine(){
		int c;
		while(!isAlNumOrSpace(c = read()));
		StringBuilder build = new StringBuilder();
		build.append((char)c);
		while(isAlNumOrSpace(c = read())){
			build.append((char)c);
		}
		return build.toString();
	}
	private static byte[] inputBuffer = new byte[1024];
	private static int bufferLength = 0;
	private static int bufferIndex = 0;
	
	private static int read(){
		if(bufferLength < 0) throw new RuntimeException();
		if(bufferIndex >= bufferLength){
			try{
				bufferLength = System.in.read(inputBuffer);
				bufferIndex = 0;
			}catch(IOException e){
				throw new RuntimeException(e);
			}
			if(bufferLength <= 0) return (bufferLength = -1);
		}
		return inputBuffer[bufferIndex++];
	}
	
	private static boolean isAlNum(int c){
		return '!' <= c && c <= '~';
	}
	private static boolean isAlNumOrSpace(int c){
		return isAlNum(c) || c == ' ' || c == '\t';
	}
	void debug(Object... os){
		out.println(Arrays.deepToString(os));
	}
}

class Dice{
	int[][] fa = new int[][]{
		{5, 3, 4, 2, 5, 3, 4, 2},
		{1, 5, 0, 4, 1, 5, 0, 4},
		{3, 1, 2, 0, 3, 1, 2, 0},
	};
	
	char[] fc = "lrbfdu".toCharArray();

	int[] f;
	
	Dice(int... fa){
		f = fa;
	}
	
	int[] tmp = new int[4];
	//a: x���c0 y���c1 z���c2
	//ta:�]�����
	void rot(int a, int ta){
		int t = (ta % 4 + 4) % 4;
		for(int i=0;i<4;i++)tmp[i] = f[fa[a][t+i]];
		for(int i=0;i<4;i++)f[fa[a][i]] = tmp[i];
	}
	
	int get(char c){
		for(int i=0;i<6;i++)if(c == fc[i])return f[i];
		return -1;
	}
	
	public String toString(){
		return java.util.Arrays.toString(f);
	}
	
	/* ����
	//(l, r, b, f, d, u) = (0, 1, 2, 3, 4 ,5)
	x:
	 	0: u=u f=f d=d b=b	ufdb = [ufdb]ufdb
		1: u=f f=d d=b b=u	ufdb = u[fdbu]fdb
		2: u=d f=b d=u b=f	ufdb = uf[dbuf]db
		3: u=b f=u d=f b=d	ufdb = ufd[bufd]b
		
		->53425342
	y:
		0: r=r u=u l=l d=d	ruld = [ruld]ruld
		1: r=u u=l l=d d=r	ruld = r[uldr]uld
		2: r=l u=d l=r d=u	ruld = ru[ldru]ld
		3: r=d u=r l=u d=l	ruld = rul[drul]d
		
		->15041504
	z:
		0: f=f r=r b=b l=l	frbl = [frbl]frbl
		1: f=r r=b b=l l=f	frbl = f[rblf]rbl
		2: f=b r=l b=f l=r	frbl = fr[blfr]bl
		3: f=l r=f b=r l=b	frbl = frb[lfrb]l
		
		->31203120
	*/

}

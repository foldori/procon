import java.util.*;

class Main{
	
	Scanner sc;
	
	char P, Q, R;
	String line;
	int index;
	
	void run() {
		while(!((line = sc.next()).equals("."))){
			int n = 0;
			for(int p=0;p<3;p++){
				for(int q=0;q<3;q++){
					for(int r=0;r<3;r++){
						index = 0;
						P = (char)(p + '0');
						Q = (char)(q + '0');
						R = (char)(r + '0');
//						debug(P, Q, R);
						if(formula() == '2'){
							n++;
						}
					}
				}
			}
			System.out.println(n);
		}
	}
	
	char formula(){
		char next = line.charAt(index);
//		System.out.println(next);
		index++;
		if(next == 'P'){
			return P;
		}else if(next == 'Q'){
			return Q;
		}else if(next == 'R'){
			return R;
		}else if(next == '0' || next == '1' || next == '2'){
			return next;
		}else if(next == '-'){
			return not(formula());
		}else if(next == '('){
			char c1 = formula();
			char op = line.charAt(index);
			index++;
			char c2 = formula();
			index++;
			if(op == '*'){
				return and(c1, c2);
			}else{
				return or(c1, c2);
			}
		}else{
			return 'e';
		}
	}
	char and(char c1, char c2){
		if(c1 == '2' && c2 == '2'){
			return '2';
		}else if(c1 == '0' || c2 == '0'){
			return '0';
		}else{
			return '1';
		}
	}
	
	char or(char c1, char c2){
		if(c1 == '0' && c2 == '0'){
			return '0';
		}else if(c1 == '2' || c2 == '2'){
			return '2';
		}else{
			return '1';
		}
	}
	
	char not(char c){
		if(c == '0')return '2';
		else if(c == '1')return '1';
		else return '0';
	}
	
	Main () {
		sc = new Scanner(System.in);
	}
	
	int ni() {
		return sc.nextInt();
	}
	
	public static void main(String[] args){
		new Main().run();
	}
	
	void debug(Object... os){
		System.out.println(Arrays.deepToString(os));
	}
}
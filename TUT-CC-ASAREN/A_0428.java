import java.util.*;

class Main{
	
	Scanner sc;
	
	void run() {
		int n;
		while(true){
			n = ni();
			if(n == 0)break;
			
			int cnt = 0;
			for(int i=n+1;i<=2*n;i++){
				if(isPrime(i))cnt++;
			}
			System.out.println(cnt);
		}
	}
	
	boolean isPrime(int n){
		for(int i=2;i*i<=n;i++){
			if(n % i == 0)return false;
		}
		return true;
	}
	
	Main () {
		sc = new Scanner(System.in);
	}
	
	int ni() {
		return sc.nextInt();
	}
	
	public static void main(String[] args){
		new Main().run();
	}
	
	
	
	void debug(Object... os){
		System.out.println(Arrays.deepToString(os));
	}
}
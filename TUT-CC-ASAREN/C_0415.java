import java.util.*;

class Main{
	
	Scanner sc;
	
	void run() {
		int n = ni();
		for(int i=0;i<n;i++){
			String a = sc.next();
			String b = sc.next();
			System.out.println(num2str(str2num(a)+str2num(b)));
		}
	}
	
	int str2num(String s){
		//m, c, x, i
		int[] nums = new int[4];
		for(int i=0;i<s.length();i++){
			if(s.charAt(i) == 'm'){
				if(i==0){
					nums[0] = 1;
				}else{
					if('0' <= s.charAt(i-1) && s.charAt(i-1) <= '9'){
						nums[0] = s.charAt(i-1) - '0';
					}else{
						nums[0] = 1;
					}
				}
			}
			if(s.charAt(i) == 'c'){
				if(i==0){
					nums[1] = 1;
				}else{
					if('0' <= s.charAt(i-1) && s.charAt(i-1) <= '9'){
						nums[1] = s.charAt(i-1) - '0';
					}else{
						nums[1] = 1;
					}
				}
			}
			if(s.charAt(i) == 'x'){
				if(i==0){
					nums[2] = 1;
				}else{
					if('0' <= s.charAt(i-1) && s.charAt(i-1) <= '9'){
						nums[2] = s.charAt(i-1) - '0';
					}else{
						nums[2] = 1;
					}
				}
			}
			if(s.charAt(i) == 'i'){
				if(i==0){
					nums[3] = 1;
				}else{
					if('0' <= s.charAt(i-1) && s.charAt(i-1) <= '9'){
						nums[3] = s.charAt(i-1) - '0';
					}else{
						nums[3] = 1;
					}
				}
			}

		}
//		debug(nums);
		return nums[0] * 1000 + nums[1] * 100 + nums[2] * 10 + nums[3];
	}
	
	String num2str(int n){
//		int[] nums = {n % 10, (n/10) % 10, (n/100)%10, (n/1000)};
		int[] nums = {n/1000, (n/100)%10, (n/10)%10, n%10};
//		debug(n);
//		debug(nums);
		String result = "";
		if(nums[0] > 0)result += (nums[0]==1?"":String.format("%d",nums[0])) + "m";
		if(nums[1] > 0)result += (nums[1]==1?"":String.format("%d",nums[1])) + "c";
		if(nums[2] > 0)result += (nums[2]==1?"":String.format("%d",nums[2])) + "x";
		if(nums[3] > 0)result += (nums[3]==1?"":String.format("%d",nums[3])) + "i";
		return result;
	}
	
	Main () {
		sc = new Scanner(System.in);
	}
	
	int ni() {
		return sc.nextInt();
	}
	
	public static void main(String[] args){
		new Main().run();
	}
	
	
	
	void debug(Object... os){
		System.out.println(Arrays.deepToString(os));
	}
}
import java.io.*;
import java.util.*;

class Main{
	public void run(){
		int n;
		n = ni();
		for(int i=0;i<n;i++){
			StringBuilder str = new StringBuilder(next());
			HashSet<String> set = new HashSet<String>();
			for(int j=1;j<str.length();j++){
				StringBuilder ls = new StringBuilder(str.substring(0, j));
				StringBuilder rs = new StringBuilder(str.substring(j, str.length()));
				String s1 = new StringBuilder(ls).append(new StringBuilder(rs)).toString();
				String s2 = new StringBuilder(ls).append(new StringBuilder(rs).reverse()).toString();
				String s3 = new StringBuilder(ls).reverse().append(new StringBuilder(rs)).toString();
				String s4 = new StringBuilder(ls).reverse().append(new StringBuilder(rs).reverse()).toString();
				set.add(s1);
				set.add(s2);
				set.add(s3);
				set.add(s4);
				s1 = new StringBuilder(rs).append(new StringBuilder(ls)).toString();
				s2 = new StringBuilder(rs).append(new StringBuilder(ls).reverse()).toString();
				s3 = new StringBuilder(rs).reverse().append(new StringBuilder(ls)).toString();
				s4 = new StringBuilder(rs).reverse().append(new StringBuilder(ls).reverse()).toString();
				set.add(s1);
				set.add(s2);
				set.add(s3);
				set.add(s4);
			}
			System.out.println(set.size());
		}
	}

	private static PrintWriter out;
	public static void main(String[] args){
		out = new PrintWriter(System.out);
		new Main().run();
		out.flush();
	}

	public Main(){
	}

	int ni(){
		int num = 0;
		String str = next();
		boolean minus = false;
		int i = 0;
		if(str.charAt(0) == '-'){
			minus = true;
			i++;
		}
		int len = str.length();
		for(;i < len; i++){
			char c = str.charAt(i);
			if(!('0' <= c && c <= '9')) throw new RuntimeException();
			num = num * 10 + (c - '0');
		}
		return minus ? -num : num;	}

	long nl(){
		long num = 0;
		String str = next();
		boolean minus = false;
		int i = 0;
		if(str.charAt(0) == '-'){
			minus = true;
			i++;
		}
		int len = str.length();
		for(;i < len; i++){
			char c = str.charAt(i);
			if(!('0' <= c && c <= '9')) throw new RuntimeException();
			num = num * 10l + (c - '0');
		}
		return minus ? -num : num;
	}

	String next(){
		int c;
		while(!isAlNum(c = read()));
		StringBuilder build = new StringBuilder();
		build.append((char)c);
		while(isAlNum(c = read())){
			build.append((char)c);
		}
		return build.toString();
	}
	private static byte[] inputBuffer = new byte[1024];
	private static int bufferLength = 0;
	private static int bufferIndex = 0;
	
	private static int read(){
		if(bufferLength < 0) throw new RuntimeException();
		if(bufferIndex >= bufferLength){
			try{
				bufferLength = System.in.read(inputBuffer);
				bufferIndex = 0;
			}catch(IOException e){
				throw new RuntimeException(e);
			}
			if(bufferLength <= 0) return (bufferLength = -1);
		}
		return inputBuffer[bufferIndex++];
	}
	
	private static boolean isAlNum(int c){
		return '!' <= c && c <= '~';
	}
	void debug(Object... os){
		out.println(Arrays.deepToString(os));
	}
}
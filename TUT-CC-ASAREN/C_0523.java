import java.io.*;
import java.util.*;

class Main{
	public void run(){
		int n;
		while(true){
			n = ni();
			if(n == 0)break;
			
			double[][] points = new double[n][2];
			for(int i=0;i<n;i++){
				points[i][0] = nd();
				points[i][1] = nd();
//				out.printf("%f%f\n", points[i][0], points[i][1]);
			}
			int max = 0;
			
			for(int i=0;i<n;i++){
				double x0 = points[i][0];
				double y0 = points[i][1];
				int tmp = 0;
				for(int j=0;j<n;j++){
					double x = points[j][0];
					double y = points[j][1];
					out.printf("%d %d\n", i, j);
					out.println(((x-x0)*(x-x0) + (y-y0)*(y-y0)));
					if(((x-x0)*(x-x0) + (y-y0)*(y-y0)) < 1){
						tmp++;
					}
				}
				max = Math.max(max, tmp-1);
			}
			out.println(max);
		}
	}

	private static PrintWriter out;
	public static void main(String[] args){
		out = new PrintWriter(System.out);
		new Main().run();
		out.flush();
	}

	public Main(){
	}

	int ni(){
		int num = 0;
		String str = next();
		boolean minus = false;
		int i = 0;
		if(str.charAt(0) == '-'){
			minus = true;
			i++;
		}
		int len = str.length();
		for(;i < len; i++){
			char c = str.charAt(i);
			if(!('0' <= c && c <= '9')) throw new RuntimeException();
			num = num * 10 + (c - '0');
		}
		return minus ? -num : num;	}

	long nl(){
		long num = 0;
		String str = next();
		boolean minus = false;
		int i = 0;
		if(str.charAt(0) == '-'){
			minus = true;
			i++;
		}
		int len = str.length();
		for(;i < len; i++){
			char c = str.charAt(i);
			if(!('0' <= c && c <= '9')) throw new RuntimeException();
			num = num * 10l + (c - '0');
		}
		return minus ? -num : num;
	}
	
	double nd(){
		return Double.parseDouble(next());
	}

	String next(){
		int c;
		while(!isAlNum(c = read()));
		StringBuilder build = new StringBuilder();
		build.append((char)c);
		while(isAlNum(c = read())){
			build.append((char)c);
		}
		return build.toString();
	}
	private static byte[] inputBuffer = new byte[1024];
	private static int bufferLength = 0;
	private static int bufferIndex = 0;
	
	private static int read(){
		if(bufferLength < 0) throw new RuntimeException();
		if(bufferIndex >= bufferLength){
			try{
				bufferLength = System.in.read(inputBuffer);
				bufferIndex = 0;
			}catch(IOException e){
				throw new RuntimeException(e);
			}
			if(bufferLength <= 0) return (bufferLength = -1);
		}
		return inputBuffer[bufferIndex++];
	}
	
	private static boolean isAlNum(int c){
		return '!' <= c && c <= '~';
	}
	void debug(Object... os){
		out.println(Arrays.deepToString(os));
	}
}
import java.util.*;

class Main{
	
	Scanner sc;
	
	void run() {
		int N, M;
		while((N=ni())!=0 && (M=ni())!=0){
			int r = ni();
			ArrayList<ArrayList<int[]>> log = new ArrayList<ArrayList<int[]>>();
			for(int i=0;i<M;i++){
				log.add(new ArrayList<int[]>());
			}
			
			int[] u_time = new int[M];
			for(int i=0;i<r;i++){
				int t = ni();
				int n = ni();
				int m = ni();
				int s = ni();
				
				if(u_time[m-1] == 0){
					u_time[m-1] = t;
				}else{
					log.get(m-1).add(new int[]{u_time[m-1], t});
				}
			}
			
			int q = ni();
			for(int i=0;i<q;i++){
				int t_s = ni();
				int t_e = ni();
				int m = ni();
				ArrayList<int[]> m_log = log.get(m-1);
				int ans = 0;
				for(int j=0;j<m_log.size();j++){
					int[] t = m_log.get(j);
					if(t_e <= t[0] || t[1] <= t_s){
					}else if(t[0] <= t_s && t_e <= t[1]){
						ans += (t[1] - t[0]);
					}else{
						if(t_s < t[0]){
							ans += (t_e - t[0]);
						}else{
							ans += (t[1] - t_s);
						}
					}
				}
				System.out.println(ans);
			}
		}
	}
	
	Main () {
		sc = new Scanner(System.in);
	}
	
	int ni() {
		return sc.nextInt();
	}
	
	public static void main(String[] args){
		new Main().run();
	}
	
	
	
	void debug(Object... os){
		System.out.println(Arrays.deepToString(os));
	}
}
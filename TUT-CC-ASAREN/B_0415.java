import java.util.*;

class Main{
	
	Scanner sc;
	int[][] map;
	int cnt;
	int[] tx = {1, 0, -1, 0, 1, 1, -1, -1};
	int[] ty = {0, 1, 0, -1, 1, -1, 1, -1};
	
	int calc(int y, int x){
		if(map[y+1][x+1] == 0){
			return 0;
		}else{
			/*
			debug(y, x);
			for(int i=0;i<map.length;i++){
				for(int j=0;j<map[0].length;j++){
					System.out.print(map[i][j]);
				}
				System.out.println();
			}
			*/
			map[y+1][x+1] = 0;
			for(int i=0;i<tx.length;i++){
				calc(y + ty[i], x + tx[i]);
			}
			
			return 1;
		}
	}
	
	void run() {
		int w, h;
		while((w=ni())!=0 && (h=ni())!=0){
			map = new int[h+2][w+2];
			for(int i=0;i<h;i++){
				for(int j=0;j<w;j++){
					map[i+1][j+1] = ni();
				}
			}
			cnt = 0;
			for(int i=0;i<h;i++){
				for(int j=0;j<w;j++){
					cnt += calc(i, j);
				}
			}
			System.out.println(cnt);
		}
	}
	
	Main () {
		sc = new Scanner(System.in);
	}
	
	int ni() {
		return sc.nextInt();
	}
	
	public static void main(String[] args){
		new Main().run();
	}
	
	
	
	void debug(Object... os){
		System.out.println(Arrays.deepToString(os));
	}
}
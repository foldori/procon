import java.util.*;

class Main{
	
	Scanner sc;
	
	void run() {
		int a, b;
		while((a=ni())!=0 && (b=ni())!=0){
			int i = 0;
			int[] p = new int[a];
			BitSet set = new BitSet(a);
			loop:
			while(true){
				if(b == 0){
					b += p[i];
					p[i] = 0;
					set.set(i, false);
				}else{
					p[i]++;
					b--;
					set.set(i, true);
				}
				
				if(set.isEmpty()){
					break loop;
				}
				i = (i + 1) % a;
			}
			System.out.println(i);
		}
	}
	
	Main () {
		sc = new Scanner(System.in);
	}
	
	int ni() {
		return sc.nextInt();
	}
	
	public static void main(String[] args){
		new Main().run();
	}
	
	
	
	void debug(Object... os){
		System.out.println(Arrays.deepToString(os));
	}
}
import java.util.*;

class Main{
	
	Scanner sc;
	
	void run() {
		int n, r;
		while(true){
			n = ni();
			r = ni();
			if(r == 0 && n == 0)break;
			
			int[] deck = new int[n];
			for(int i=0;i<n;i++){
				deck[i] = n - i;
			}
//			debug(deck);
			for(int i=0;i<r;i++){
				int p = ni();
				int c = ni();
				
				int[] tmp = new int[n];
				for(int j=0;j<n;j++){
					if(j < c){
						tmp[j] = deck[p + j - 1];
					}else if(c <= j && j < (c + p - 1)){
						tmp[j] = deck[j - c];
					}else{
						tmp[j] = deck[j];
					}
				}
				
				deck = tmp;
//				debug(deck);
			}
			System.out.println(deck[0]);
		}
	}
	
	Main () {
		sc = new Scanner(System.in);
	}
	
	int ni() {
		return sc.nextInt();
	}
	
	public static void main(String[] args){
		new Main().run();
	}
	
	
	
	void debug(Object... os){
		System.out.println(Arrays.deepToString(os));
	}
}
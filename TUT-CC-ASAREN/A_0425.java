import java.util.*;

class Main{
	
	Scanner sc;
	
	void run() {
		int n;
		while(true){
			n = ni();
			if(n == 0)break;
			
			int[] x = new int[n];
			int[] y = new int[n];
			
			for(int i=1;i<n;i++){
				int ni = ni();
				int d = ni();
				
				if(d == 0){
					x[i] = x[ni] - 1;
					y[i] = y[ni];
				}else if(d == 1){
					x[i] = x[ni];
					y[i] = y[ni] - 1;
				}else if(d == 2){
					x[i] = x[ni] + 1;
					y[i] = y[ni];
				}else if(d == 3){
					x[i] = x[ni];
					y[i] = y[ni] + 1;
				}
			}
			
//			debug(x);
//			debug(y);
			int xMin = Integer.MAX_VALUE;
			int xMax = Integer.MIN_VALUE;
			int yMin = Integer.MAX_VALUE;
			int yMax = Integer.MIN_VALUE;
			
			for(int i=0;i<n;i++){
				xMin = Math.min(xMin, x[i]);
				xMax = Math.max(xMax, x[i]);
				yMin = Math.min(yMin, y[i]);
				yMax = Math.max(yMax, y[i]);
			}
			
			System.out.printf("%d %d\n", xMax - xMin + 1, yMax - yMin + 1);
		}
	}
	
	Main () {
		sc = new Scanner(System.in);
	}
	
	int ni() {
		return sc.nextInt();
	}
	
	public static void main(String[] args){
		new Main().run();
	}
	
	
	
	void debug(Object... os){
		System.out.println(Arrays.deepToString(os));
	}
}
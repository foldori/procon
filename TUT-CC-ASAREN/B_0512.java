import java.io.*;
import java.util.*;

class Main{
	public void run(){
		int n = ni();
		for(int i=0;i<10;i++){
			int a = mcxi2int(next());
			int b = mcxi2int(next());
			out.println(int2mcxi(a+b));
		}
	}
	
	int mcxi2int(String str){
		int m, c, x, i;
		m = c = x = i = 0;
		for(int j=0;j<str.length();j++){
			char ch = str.charAt(j);
			char b = '*';
			if(j >= 1)b = str.charAt(j-1);
			
			if('0' <= b && b <= '9'){
				if(ch == 'm'){
					m = b - '0';
				}else if(ch == 'c'){
					c = b - '0';
				}else if(ch == 'x'){
					x = b - '0';
				}else if(ch == 'i'){
					i = b - '0';
				}
			}else{
				if(ch == 'm'){
					m = 1;
				}else if(ch == 'c'){
					c = 1;
				}else if(ch == 'x'){
					x = 1;
				}else if(ch == 'i'){
					i = 1;
				}				
			}
		}
		return m * 1000 + c * 100 + x * 10 + i;
	}
	
	String int2mcxi(int n){
		String m = "";
		if(n / 1000 != 0)m = ((n/1000==1)?"":n/1000+"")+"m";
		n %= 1000;
		
		String c = "";
		if(n / 100 != 0)c = ((n/100==1)?"":n/100+"")+"c";
		n %= 100;
		
		String x = "";
		if(n / 10 != 0)x = ((n/10==1)?"":n/10+"")+"x";
		n %= 10;
		
		String i = "";
		if(n != 0)i = ((n==1)?"":n+"")+"i";
	return m+c+x+i;		
	}

	private static PrintWriter out;
	public static void main(String[] args){
		out = new PrintWriter(System.out);
		new Main().run();
		out.flush();
	}

	public Main(){
	}

	int ni(){
		int num = 0;
		String str = next();
		boolean minus = false;
		int i = 0;
		if(str.charAt(0) == '-'){
			minus = true;
			i++;
		}
		int len = str.length();
		for(;i < len; i++){
			char c = str.charAt(i);
			if(!('0' <= c && c <= '9')) throw new RuntimeException();
			num = num * 10 + (c - '0');
		}
		return minus ? -num : num;	}

	long nl(){
		long num = 0;
		String str = next();
		boolean minus = false;
		int i = 0;
		if(str.charAt(0) == '-'){
			minus = true;
			i++;
		}
		int len = str.length();
		for(;i < len; i++){
			char c = str.charAt(i);
			if(!('0' <= c && c <= '9')) throw new RuntimeException();
			num = num * 10l + (c - '0');
		}
		return minus ? -num : num;
	}

	String next(){
		int c;
		while(!isAlNum(c = read()));
		StringBuilder build = new StringBuilder();
		build.append((char)c);
		while(isAlNum(c = read())){
			build.append((char)c);
		}
		return build.toString();
	}
	private static byte[] inputBuffer = new byte[1024];
	private static int bufferLength = 0;
	private static int bufferIndex = 0;
	
	private static int read(){
		if(bufferLength < 0) throw new RuntimeException();
		if(bufferIndex >= bufferLength){
			try{
				bufferLength = System.in.read(inputBuffer);
				bufferIndex = 0;
			}catch(IOException e){
				throw new RuntimeException(e);
			}
			if(bufferLength <= 0) return (bufferLength = -1);
		}
		return inputBuffer[bufferIndex++];
	}
	
	private static boolean isAlNum(int c){
		return '!' <= c && c <= '~';
	}
	void debug(Object... os){
		out.println(Arrays.deepToString(os));
	}
}
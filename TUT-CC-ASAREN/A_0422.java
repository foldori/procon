import java.util.*;

class Main{
	
	Scanner sc;
	
	void run() {
		int n;
		while(true){
			n = ni();
			if(n == 0)break;
			int w = ni();
			int h = ni();
			int[][] map = new int[h+1][w+1];
			
			for(int i=0;i<n;i++){
				int x = ni();
				int y = ni();
				map[y][x] = 1;
			}
			
			int[][] sum = new int[h+1][w+1];
			
			for(int i=1;i<=h;i++){
				for(int j=1;j<=w;j++){
					sum[i][j] = map[i][j] + sum[i-1][j] + sum[i][j-1] - sum[i-1][j-1];
				}
			}
			
			for(int i=0;i<=h;i++){
				debug(map[i]);
			}
			System.out.println();
			for(int i=0;i<=h;i++){
				debug(sum[i]);
			}
			int s = ni();
			int t = ni();
			int max = 0;
			for(int i=0;i<=h-t;i++){
				for(int j=0;j<=w-s;j++){
					int x1 = j;
					int y1 = i;
					int x2 = j + s;
					int y2 = i + t;
					int tmp = sum[y2][x2] - sum[y1][x2] - sum[y2][x1] + sum[x1][y1];
					debug(tmp, x1, y1, x2, y2);
					max = Math.max(tmp, max);
				}
			}
			System.out.println(max);
		}
	}
	
	Main () {
		sc = new Scanner(System.in);
	}
	
	int ni() {
		return sc.nextInt();
	}
	
	public static void main(String[] args){
		new Main().run();
	}
	
	
	
	void debug(Object... os){
		System.out.println(Arrays.deepToString(os));
	}
}
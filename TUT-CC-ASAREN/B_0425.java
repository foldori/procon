import java.util.*;

class Main{
	
	Scanner sc;
	
	int[][] a;
	int h;
	void run() {
		while(true){
			h = ni();
			if(h == 0)break;
			a = new int[h][5];
			for(int i=0;i<h;i++){
				a[i][0] = ni();
				a[i][1] = ni();
				a[i][2] = ni();
				a[i][3] = ni();
				a[i][4] = ni();
			}
			int score = 0;
			int tmp;
			while((tmp = calc()) > 0){
				score += tmp;
			}
			System.out.println(score);
		}
	}
	
	int calc(){
		int score = 0;
		for(int i=0;i<h;i++){
			//0=1=2=3=4
			if(a[i][0] == a[i][1] && 
			a[i][1] == a[i][2] && 
			a[i][2] == a[i][3] && 
			a[i][3] == a[i][4]){
				score += a[i][0] * 5;
				a[i][0] = a[i][1] = a[i][2] = a[i][3] = a[i][4] = 0;
				continue;
			}
			//0=1=2=3
			if(a[i][0] == a[i][1] && 
			a[i][1] == a[i][2] && 
			a[i][2] == a[i][3]){
				score += a[i][0] * 4;
				a[i][0] = a[i][1] = a[i][2] = a[i][3] = 0;
				continue;
			}
			//1=2=3=4
			if(a[i][1] == a[i][2] && 
			a[i][2] == a[i][3] &&
			a[i][3] == a[i][4]){
				score += a[i][1] * 4;
				a[i][1] = a[i][2] = a[i][3] = a[i][4] = 0;
				continue;
			}
			//0=1=2
			if(a[i][0] == a[i][1] && a[i][1] == a[i][2]){
				score += a[i][0] * 3;
				a[i][0] = a[i][1] = a[i][2] = 0;
				continue;
			}
			//1=2=3
			if(a[i][1] == a[i][2] && a[i][2] == a[i][3]){
				score += a[i][1] * 3;
				a[i][1] = a[i][2] = a[i][3] = 0;
				continue;
			}
			//2=3=4
			if(a[i][2] == a[i][3] && a[i][3] == a[i][4]){
				score += a[i][2] * 3;
				a[i][2] = a[i][3] = a[i][4] = 0;
				continue;
			}
		}
		for(int i=0;i<5;i++){
			for(int j=h-2;j>=0;j--){
				if(a[j][i] != 0 && a[j+1][i] == 0){
					int t = j + 1;
					while(t+1 < h && a[t + 1][i] == 0)t++;
					a[t][i] = a[j][i];
					a[j][i] = 0;
				}
			}
		}
//		debug(a);
		return score;
	}
	
	Main () {
		sc = new Scanner(System.in);
	}
	
	int ni() {
		return sc.nextInt();
	}
	
	public static void main(String[] args){
		new Main().run();
	}
	
	
	
	void debug(Object... os){
		System.out.println(Arrays.deepToString(os));
	}
}
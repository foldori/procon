import java.util.*;

class Main{
	class Cake{
		int w;
		int d;
		
		public Cake(int w, int d){
			this.w = w;
			this.d = d;
		}
		public Cake(){}
		
		Cake cut(int s){
			s = s % (w + d + w + d);
			
			Cake newCake = new Cake(w, d);
			if(0 <= s && s < w){//上
				int w1 = s;
				int w2 = this.w - w1;
				this.w = w1;
				newCake.w = w2;
			}else if(w <= s && s < (w + d)){//右
				int d1 = s - w;
				int d2 = this.d - d1;
				this.d = d1;
				newCake.d = d2;
			}else if((w + d) <= s && s < (w + d + w)){//下
				int w1 = s - w - d;
				int w2 = this.w - w1;
				this.w = w1;
				newCake.w = w2;
			}else if((w + d + w) <= s && s < (w + d + w + d)){//左
				int d1 = s - w - d - w;
				int d2 = this.d - d1;
				this.d = d1;
				newCake.d = d2;
			}
			
			return newCake;
		}
		
		int area(){
			return w * d;
		}
	}
	
	Scanner sc;
	
	void run() {
		int n, w, d;
		while(true){
			n = ni();
			w = ni();
			d = ni();
			if((n|w|d)==0)break;
			
			ArrayList<Cake> list = new ArrayList<Cake>();
			list.add(new Cake(w, d));
			
			for(int i=0;i<n;i++){
				ArrayList<Cake> newList = new ArrayList<Cake>();
				int p = ni();
				int s = ni();
				
				Cake c1 = null;
				for(int j=0;j<list.size();j++){
					if(j + 1 == p){
						c1 = list.get(j);
					}else{
						newList.add(list.get(j));
					}
				}
				
				Cake c2 = c1.cut(s);
				
				if(c1.area() > c2.area()){
					newList.add(c2);
					newList.add(c1);
				}else{
					newList.add(c1);
					newList.add(c2);					
				}
				
				list = newList;
			}
			
			Collections.sort(list, new Comparator<Cake>(){
				public int compare(Cake c1, Cake c2){
					return c1.area() - c2.area();
				}
			});
			for(int i=0;i<list.size();i++){
				System.out.printf("%d", list.get(i).area());
				if(i != list.size() - 1){
					System.out.print(" ");
				}else{
					System.out.println();
				}
			}
			
		}
	}
	
	Main () {
		sc = new Scanner(System.in);
	}
	
	int ni() {
		return sc.nextInt();
	}
	
	public static void main(String[] args){
		new Main().run();
	}
	
	
	
	void debug(Object... os){
		System.out.println(Arrays.deepToString(os));
	}
}

import java.util.*;

class Main{
	class Team{
		//ID
		int id;

		//waの数
		int[] was;

		//正答にかかった時間
		int[] acTime;

		//正答数
		int acNum;

		//経過時間
		int time;

		public Team(int pNum, int id){
			was = new int[pNum];
			acTime = new int[pNum];
			for(int i=0;i<pNum;i++)acTime[i] = -1;
			this.id = id;
		}
	}

	Scanner sc;

	void run() {
		int M, T, P, R;
		while(true){
			M = ni();
			T = ni();
			P = ni();
			R = ni();
			if((M|T|P|R)==0)break;

			Team[] teams = new Team[T];
			for(int i=0;i<T;i++)teams[i] = new Team(P, i+1);

			for(int i=0;i<R;i++){
				int m = ni();
				int t = ni();
				int p = ni();
				int j = ni();

				if(j != 0){
					teams[t-1].was[p-1]++;
				}else{
					teams[t-1].acTime[p-1] = m;

				}
			}

			for(int i=0;i<T;i++){
				Team tmp = teams[i];

				//AC数・経過時間計算
				for(int j=0;j<P;j++){
					if(tmp.acTime[j] != -1){
						tmp.acNum++;
						tmp.time += tmp.was[j] * 20 + tmp.acTime[j];
					}
				}
			}
//			for(int i=0;i<T;i++)System.out.printf("%d %d %d\n", teams[i].id, teams[i].acNum, teams[i].time);

			Arrays.sort(teams, new Comparator<Team>(){
				public int compare(Team t1, Team t2){
					if(t1.acNum != t2.acNum){
						return t2.acNum - t1.acNum;
					}else{
						if(t1.time != t2.time){
							return t1.time - t2.time;
						}else{
							return t2.id - t1.id;
						}
					}
				}
			});

			for(int i=0;i<T;i++){
				if(i==0){
					System.out.print(teams[i].id);
				}else{
					if(teams[i].acNum == teams[i-1].acNum && teams[i].time == teams[i-1].time){
						System.out.print("=");
					}else{
						System.out.print(",");
					}
					System.out.print(teams[i].id);
				}
			}
			System.out.println();
		}
	}

	Main () {
		sc = new Scanner(System.in);
	}

	int ni() {
		return sc.nextInt();
	}

	public static void main(String[] args){
		new Main().run();
	}



	void debug(Object... os){
		System.out.println(Arrays.deepToString(os));
	}
}
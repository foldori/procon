class Dice{
	int[][] fa = new int[][]{
		{5, 3, 4, 2, 5, 3, 4, 2},
		{1, 5, 0, 4, 1, 5, 0, 4},
		{3, 1, 2, 0, 3, 1, 2, 0},
	};
	
	char[] fc = "lrbfdu".toCharArray();

	int[] f;
	
	Dice(int... fa){
		f = fa;
	}
	
	int[] tmp = new int[4];
	//x���c0 y���c1 z���c2
	void rot(int a, int ta){
		int t = (ta % 4 + 4) % 4;
		for(int i=0;i<4;i++)tmp[i] = f[fa[a][t+i]];
		for(int i=0;i<4;i++)f[fa[a][i]] = tmp[i];
	}
	
	int get(char c){
		for(int i=0;i<6;i++)if(c == fc[i])return f[i];
		return -1;
	}
	
	public String toString(){
		return java.util.Arrays.toString(f);
	}
	
	/* ����
	//(l, r, b, f, d, u) = (0, 1, 2, 3, 4 ,5)
	x:
	 	0: u=u f=f d=d b=b	ufdb = [ufdb]ufdb
		1: u=f f=d d=b b=u	ufdb = u[fdbu]fdb
		2: u=d f=b d=u b=f	ufdb = uf[dbuf]db
		3: u=b f=u d=f b=d	ufdb = ufd[bufd]b
		
		->53425342
	y:
		0: r=r u=u l=l d=d	ruld = [ruld]ruld
		1: r=u u=l l=d d=r	ruld = r[uldr]uld
		2: r=l u=d l=r d=u	ruld = ru[ldru]ld
		3: r=d u=r l=u d=l	ruld = rul[drul]d
		
		->15041504
	z:
		0: f=f r=r b=b l=l	frbl = [frbl]frbl
		1: f=r r=b b=l l=f	frbl = f[rblf]rbl
		2: f=b r=l b=f l=r	frbl = fr[blfr]bl
		3: f=l r=f b=r l=b	frbl = frb[lfrb]l
		
		->31203120
	*/

}

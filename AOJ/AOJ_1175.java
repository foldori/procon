import java.io.*;
import java.util.*;

class Main{
	class Disc{
		HashSet<Disc> parents;
		int x;
		int y;
		int r;
		int c;
		int id;
		
		public Disc(int x, int y, int r, int c, int id){
			this.x = x;
			this.y = y;
			this.r = r;
			this.c = c;
			this.id = id;
			
			parents = new HashSet<Disc>();
		}
	}
	
	Disc[] discs;
	public void run(){
		int n;
		while(true){
			n=ni();
			if(n == 0)break;
			
			discs = new Disc[n];
			for(int i=0;i<n;i++){
				int x = ni();
				int y = ni();
				int r = ni();
				int c = ni();
				discs[i] = new Disc(x, y, r, c, i);
				
			}
			
			for(int i=0;i<n;i++){
				for(int j=0;j<i;j++){
					int x1 = discs[i].x;
					int y1 = discs[i].y;
					int r1 = discs[i].r;
					
					int x2 = discs[j].x;
					int y2 = discs[j].y;
					int r2 = discs[j].r;
					if(((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)) < (r1 + r2)*(r1 + r2)){
						if(j > i){
							discs[j].parents.add(discs[i]);
						}else{
							discs[i].parents.add(discs[j]);
						}
					}
				}
			}
			
			out.println(solve(new HashSet<Integer>()));
		}
	}
	
	
	int solve(HashSet<Integer> rmSet){
		/*
		out.print("rmSet:");
		for(int id : rmSet){
			out.print(" "+id);
		}
		out.println();
		*/
		
		//rmSetに含まれるディスクは既に取っている
		//何枚取れるか集計
		int[] gn = new int[4];
		HashMap<Integer, ArrayList<Integer>> set = new HashMap<Integer, ArrayList<Integer>>();
		for(Disc d : discs){
			if(rmSet.contains(d.id))continue;
			boolean removal = true;
			for(Disc p : d.parents){
				if(!rmSet.contains(p.id)){
					removal = false;
				}
			}
			
			//色ごとにidのリストを作る
			if(removal){
				gn[d.c-1]++;
				if(set.containsKey(d.c)){
					set.get(d.c).add(d.id);
				}else{
					ArrayList<Integer> newList = new ArrayList<Integer>();
					newList.add(d.id);
					set.put(d.c, newList);
				}
			}
		}
		
//		out.print("gn:");
//		debug(gn);
		boolean end = true;
		for(int i=0;i<4;i++){
			if(gn[i]>1)end = false;
		}
		if(end)return 0;
		
		int max = 0;
		for(Map.Entry<Integer, ArrayList<Integer>> m : set.entrySet()){
			ArrayList<Integer> gList = m.getValue();
//			debug(gList.toArray());
			int color = m.getKey();
			if(gList.size() <= 1)continue;
			
			for(int i=0;i<gList.size();i++){
				for(int j=0;j<i;j++){
					int id1 = gList.get(i);
					int id2 = gList.get(j);
					rmSet.add(id1);
					rmSet.add(id2);
					
					max = Math.max(max, solve(rmSet));
					
					rmSet.remove(id1);
					rmSet.remove(id2);
				}
			}
		}
		
		return max + 2;
	}

	private static PrintWriter out;
	public static void main(String[] args){
		long now = System.currentTimeMillis();
		
		out = new PrintWriter(System.out);
		new Main().run();
		
		out.println(now - System.currentTimeMillis());
		
		out.flush();
		
	}

	public Main(){
	}

	int ni(){
		int num = 0;
		String str = next();
		boolean minus = false;
		int i = 0;
		if(str.charAt(0) == '-'){
			minus = true;
			i++;
		}
		int len = str.length();
		for(;i < len; i++){
			char c = str.charAt(i);
			if(!('0' <= c && c <= '9')) throw new RuntimeException();
			num = num * 10 + (c - '0');
		}
		return minus ? -num : num;	}

	long nl(){
		long num = 0;
		String str = next();
		boolean minus = false;
		int i = 0;
		if(str.charAt(0) == '-'){
			minus = true;
			i++;
		}
		int len = str.length();
		for(;i < len; i++){
			char c = str.charAt(i);
			if(!('0' <= c && c <= '9')) throw new RuntimeException();
			num = num * 10l + (c - '0');
		}
		return minus ? -num : num;
	}

	double nd(){
		return Double.parseDouble(next());
	}

	String next(){
		int c;
		while(!isAlNum(c = read()));
		StringBuilder build = new StringBuilder();
		build.append((char)c);
		while(isAlNum(c = read())){
			build.append((char)c);
		}
		return build.toString();
	}
	String nextLine(){
		int c;
		while(!isAlNumOrSpace(c = read()));
		StringBuilder build = new StringBuilder();
		build.append((char)c);
		while(isAlNumOrSpace(c = read())){
			build.append((char)c);
		}
		return build.toString();
	}
	private static byte[] inputBuffer = new byte[1024];
	private static int bufferLength = 0;
	private static int bufferIndex = 0;
	
	private static int read(){
		if(bufferLength < 0) throw new RuntimeException();
		if(bufferIndex >= bufferLength){
			try{
				bufferLength = System.in.read(inputBuffer);
				bufferIndex = 0;
			}catch(IOException e){
				throw new RuntimeException(e);
			}
			if(bufferLength <= 0) return (bufferLength = -1);
		}
		return inputBuffer[bufferIndex++];
	}
	
	private static boolean isAlNum(int c){
		return '!' <= c && c <= '~';
	}
	private static boolean isAlNumOrSpace(int c){
		return isAlNum(c) || c == ' ' || c == '\t';
	}
	void debug(Object... os){
		out.println(Arrays.deepToString(os));
	}
}
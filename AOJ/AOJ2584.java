import java.io.*;
import java.util.*;

class Main{
	ArrayList<Integer> pos;
	String s;
	String min = null;
	public void run(){
		while(true){
			s = next();
			if(s.equals("."))break;
			
			int n = 0;
			pos = new ArrayList<Integer>();
			for(int i=0;i<s.length();i++){
				if(s.charAt(i) == '?'){
					n++;
					pos.add(i);
				}
			}
			min = null;
			char[] arr = new char[n];
			rec(n, arr);
			
			out.println(min);
		}
	}
	
	void rec(int n, char[] arr){
		if(n == 0){
			char[] str_c = new char[s.length()];
			for(int i=0;i<s.length();i++){
				str_c[i] = s.charAt(i);
			}
			for(int i=0;i<arr.length;i++){
				str_c[pos.get(i)] = arr[i];
			}
			str = new String(str_c);
			str = parse(str+'$');
			
			if(min == null){
				min = str;
			}else{
				if(min.compareTo(str) > 0){
					min = str;
				}
			}
		}else{
			for(char c = 'A';c<='Z';c++){
				arr[n-1] = c;
				rec(n-1, arr);
			}
		}
	}
/*
<Cipher> ::= <String><Cipher> | eps
<String> ::= <Letter> | '['<Cipher>']'
<Letter> ::= '+'<Letter> | '-'<Letter> |
             'A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'I' | 'J' | 'K' | 'L' | 'M' |
             'N' | 'O' | 'P' | 'Q' | 'R' | 'S' | 'T' | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z'


<cipher>, <string><cipher> : [ + - A-Z
<cipher>, eps : ]
<String>, <Letter> : + - A-Z
<String>, [<Cipher>] : [
<Letter>, +<Letter> : +
<Letter>, -<Letter> : -
<Letter>, A-Z : A-Z
*/
	String str;
	int idx;
	char next;
	boolean hasNext;
	void getNext(){
		if(idx < str.length()){
			next = str.charAt(idx);
//			out.println("next:"+next);
			idx++;
		}
	}
	
	String parse(String input){
		str = input;
		idx = 0;
		getNext();
		hasNext = true;
		return cipher();
	}
	
	/*
<cipher>, <string><cipher> : [ + - A-Z
<cipher>, eps : ]
	*/
	String cipher(){
//		out.println("cipher_next:"+next);
		String tmp_c = "";
		if(
			next == '[' ||
			next == '+' ||
			next == '-' ||
			('A' <= next && next <= 'Z')){
			tmp_c += string();
			tmp_c += cipher();
		}else if(next == ']' || next == '$'){
		}
//		out.println("cipher:"+tmp_c);
		return tmp_c;
	}
	/*
<String>, <Letter> : + - A-Z
<String>, [<Cipher>] : [
	*/
	String string(){
		String tmp_c = "";
		if(
			next == '+' ||
			next == '-' ||
			('A' <= next && next <= 'Z')){
				tmp_c += letter();
		}else if(next == '['){
			getNext();
			tmp_c += reverse(cipher());
			getNext();
		}
//		out.println("string:"+tmp_c);
		return tmp_c;
	}
/*
<Letter>, +<Letter> : +
<Letter>, -<Letter> : -
<Letter>, A-Z : A-Z
*/
	char letter(){
		char tmp = '#';
		if(next == '+'){
			getNext();
			tmp = letter();
			tmp = (char)(((tmp - 'A' + 1) % 26) + 'A');
		}else if(next == '-'){
			getNext();
			tmp = letter();
			tmp = (char)(((tmp - 'A' + 26) - 1) % 26 + 'A');
		}else if('A' <= next && next <= 'Z'){
			tmp = next;
			getNext();
		}
//		out.println("letter:"+tmp);
		return tmp;
	}
	
	String reverse(String input){
		String r = "";
		for(int i=0;i<input.length();i++){
			r += input.charAt(input.length() - i - 1);
		}
		return r;
	}

	private static PrintWriter out;
	public static void main(String[] args){
		out = new PrintWriter(System.out);
		new Main().run();
		out.flush();
	}

	public Main(){
	}

	int ni(){
		int num = 0;
		String str = next();
		boolean minus = false;
		int i = 0;
		if(str.charAt(0) == '-'){
			minus = true;
			i++;
		}
		int len = str.length();
		for(;i < len; i++){
			char c = str.charAt(i);
			if(!('0' <= c && c <= '9')) throw new RuntimeException();
			num = num * 10 + (c - '0');
		}
		return minus ? -num : num;	}

	long nl(){
		long num = 0;
		String str = next();
		boolean minus = false;
		int i = 0;
		if(str.charAt(0) == '-'){
			minus = true;
			i++;
		}
		int len = str.length();
		for(;i < len; i++){
			char c = str.charAt(i);
			if(!('0' <= c && c <= '9')) throw new RuntimeException();
			num = num * 10l + (c - '0');
		}
		return minus ? -num : num;
	}

	double nd(){
		return Double.parseDouble(next());
	}

	String next(){
		int c;
		while(!isAlNum(c = read()));
		StringBuilder build = new StringBuilder();
		build.append((char)c);
		while(isAlNum(c = read())){
			build.append((char)c);
		}
		return build.toString();
	}
	String nextLine(){
		int c;
		while(!isAlNumOrSpace(c = read()));
		StringBuilder build = new StringBuilder();
		build.append((char)c);
		while(isAlNumOrSpace(c = read())){
			build.append((char)c);
		}
		return build.toString();
	}
	private static byte[] inputBuffer = new byte[1024];
	private static int bufferLength = 0;
	private static int bufferIndex = 0;
	
	private static int read(){
		if(bufferLength < 0) throw new RuntimeException();
		if(bufferIndex >= bufferLength){
			try{
				bufferLength = System.in.read(inputBuffer);
				bufferIndex = 0;
			}catch(IOException e){
				throw new RuntimeException(e);
			}
			if(bufferLength <= 0) return (bufferLength = -1);
		}
		return inputBuffer[bufferIndex++];
	}
	
	private static boolean isAlNum(int c){
		return '!' <= c && c <= '~';
	}
	private static boolean isAlNumOrSpace(int c){
		return isAlNum(c) || c == ' ' || c == '\t';
	}
	void debug(Object... os){
		out.println(Arrays.deepToString(os));
	}
}

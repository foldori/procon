import java.io.*;
import java.util.*;

class Main{
	String line;
	int idx;
	public void run(){
		int n=ni();
		for(int i=0;i<n;i++){
			line = next();
			idx = 0;
			out.println(div());
		}
	}
	/*
	<number> ::= 0-9<number> | eps
	<div> ::= [<div_d>]
	<div_d> ::= <number> | <div><div_d>

	<number>, 0-9<number> : 0-9
	<number>, eps : ]
	<div>, [<div_d>] : [
	<div_d>, <number> : 0-9
	<div_d>, <div><div_d> : [

	*/
	int t;
	int div(){
		int h = 0;
		char next = line.charAt(idx);
		if(next == '['){
			idx++;
			ArrayList<Integer> tmp = div_d();
			if(tmp.size() == 1){
				h = tmp.get(0);
			}else{
				Collections.sort(tmp);
				int s = tmp.size() / 2 + 1;
				for(int i=0;i<s;i++){
					h += tmp.get(i);
				}
			}
			idx++;
		}
		return h;
	}

	ArrayList<Integer> div_d(){
		ArrayList<Integer> list = new ArrayList<Integer>();
		char next = line.charAt(idx);
		if('0' <= next && next <= '9'){
			int h = Integer.valueOf(number());
			h = h / 2 + 1;
			list.add(h);
		}else if(next == '['){
			list.add(div());
			list.addAll(div_d());
		}
		return list;
	}

	String number(){
		String num = "";
		char next = line.charAt(idx);
		if('0' <= next && next <= '9'){
			idx++;
			num += next;
			String a = number();
			if(!a.equals("")){
				num += a;
			}
		}else if(next == ']'){
			num = "";
		}
		return num;
	}
	private static PrintWriter out;
	public static void main(String[] args){
		out = new PrintWriter(System.out);
		new Main().run();
		out.flush();
	}

	public Main(){
	}

	int ni(){
		int num = 0;
		String str = next();
		boolean minus = false;
		int i = 0;
		if(str.charAt(0) == '-'){
			minus = true;
			i++;
		}
		int len = str.length();
		for(;i < len; i++){
			char c = str.charAt(i);
			if(!('0' <= c && c <= '9')) throw new RuntimeException();
			num = num * 10 + (c - '0');
		}
		return minus ? -num : num;	}

	long nl(){
		long num = 0;
		String str = next();
		boolean minus = false;
		int i = 0;
		if(str.charAt(0) == '-'){
			minus = true;
			i++;
		}
		int len = str.length();
		for(;i < len; i++){
			char c = str.charAt(i);
			if(!('0' <= c && c <= '9')) throw new RuntimeException();
			num = num * 10l + (c - '0');
		}
		return minus ? -num : num;
	}

	double nd(){
		return Double.parseDouble(next());
	}

	String next(){
		int c;
		while(!isAlNum(c = read()));
		StringBuilder build = new StringBuilder();
		build.append((char)c);
		while(isAlNum(c = read())){
			build.append((char)c);
		}
		return build.toString();
	}
	String nextLine(){
		int c;
		while(!isAlNumOrSpace(c = read()));
		StringBuilder build = new StringBuilder();
		build.append((char)c);
		while(isAlNumOrSpace(c = read())){
			build.append((char)c);
		}
		return build.toString();
	}
	private static byte[] inputBuffer = new byte[1024];
	private static int bufferLength = 0;
	private static int bufferIndex = 0;
	
	private static int read(){
		if(bufferLength < 0) throw new RuntimeException();
		if(bufferIndex >= bufferLength){
			try{
				bufferLength = System.in.read(inputBuffer);
				bufferIndex = 0;
			}catch(IOException e){
				throw new RuntimeException(e);
			}
			if(bufferLength <= 0) return (bufferLength = -1);
		}
		return inputBuffer[bufferIndex++];
	}
	
	private static boolean isAlNum(int c){
		return '!' <= c && c <= '~';
	}
	private static boolean isAlNumOrSpace(int c){
		return isAlNum(c) || c == ' ' || c == '\t';
	}
	void debug(Object... os){
		out.println(Arrays.deepToString(os));
	}
}
import java.io.*;
import java.util.*;

class Main{
	int[] var = new int[11];
	String line;
	int idx;
	public void run(){
		 while(true){
		 	line = next();
			if(line.equals("#"))break;
			
			boolean ans = true;
			for(int val = 0;val < (1 << 11);val++){
				int n = val;
				
				idx = 0;
				for(int i=0;i<11;i++){
					var[i] = (n & 1);
					n >>>= 1;
				}
				
				ans = ans && equation();
			}
			
			out.println(ans?"YES":"NO");
		 }
	}
	
	char nextChar(){
		char ch = line.charAt(idx);
		idx++;
		return ch;
	}
	/*
<equation> ::= <formula> "=" <formula>
<formula>  ::= "T" | "F" |
"a" | "b" | "c" | "d" | "e" | "f" |
"g" | "h" | "i" | "j" | "k" |
"-" <formula> |
"(" <formula> "*" <formula> ")" |
"(" <formula> "+" <formula> ")" |
"(" <formula> "->" <formula> ")"	*/
	boolean equation(){
		int left = formula();
		char c = nextChar();
		int right = formula();
		return left == right;
	}
	
	int formula(){
		char c = nextChar();
		int result = 0;
		if(c == 'T'){
			result = 1;
		}else if(c == 'F'){
			result = 0;
		}else if('a' <= c && c <= 'k'){
			result = var[c - 'a'];
		}else if(c == '-'){
			result = not(formula());
		}else if(c == '('){
			int a = formula();
			char op = nextChar();
			if(op == '-')nextChar();
			int b = formula();
			char rb = nextChar();
			if(op == '*')result = and(a, b);
			else if(op == '+')result = or(a, b);
			else result = imp(a, b);
		}
		return result;
	}
	
	int not(int n){return n == 0 ? 1 : 0;}
	int and(int a, int b){return a * b;}
	int or(int a, int b){return (a + b > 0) ? 1 : 0;}
	int imp(int a, int b){return (a == 1 && b == 0) ? 0 : 1;}
	
	private static PrintWriter out;
	public static void main(String[] args){
		out = new PrintWriter(System.out);
		new Main().run();
		out.flush();
	}

	public Main(){
	}

	int ni(){
		int num = 0;
		String str = next();
		boolean minus = false;
		int i = 0;
		if(str.charAt(0) == '-'){
			minus = true;
			i++;
		}
		int len = str.length();
		for(;i < len; i++){
			char c = str.charAt(i);
			if(!('0' <= c && c <= '9')) throw new RuntimeException();
			num = num * 10 + (c - '0');
		}
		return minus ? -num : num;	}

	long nl(){
		long num = 0;
		String str = next();
		boolean minus = false;
		int i = 0;
		if(str.charAt(0) == '-'){
			minus = true;
			i++;
		}
		int len = str.length();
		for(;i < len; i++){
			char c = str.charAt(i);
			if(!('0' <= c && c <= '9')) throw new RuntimeException();
			num = num * 10l + (c - '0');
		}
		return minus ? -num : num;
	}

	String next(){
		int c;
		while(!isAlNum(c = read()));
		StringBuilder build = new StringBuilder();
		build.append((char)c);
		while(isAlNum(c = read())){
			build.append((char)c);
		}
		return build.toString();
	}
	private static byte[] inputBuffer = new byte[1024];
	private static int bufferLength = 0;
	private static int bufferIndex = 0;
	
	private static int read(){
		if(bufferLength < 0) throw new RuntimeException();
		if(bufferIndex >= bufferLength){
			try{
				bufferLength = System.in.read(inputBuffer);
				bufferIndex = 0;
			}catch(IOException e){
				throw new RuntimeException(e);
			}
			if(bufferLength <= 0) return (bufferLength = -1);
		}
		return inputBuffer[bufferIndex++];
	}
	
	private static boolean isAlNum(int c){
		return '!' <= c && c <= '~';
	}
	void debug(Object... os){
		out.println(Arrays.deepToString(os));
	}
}
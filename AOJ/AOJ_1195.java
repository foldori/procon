import java.io.*;
import java.util.*;

class Main{
	TreeSet<String> stList, endList;
	int count = 0;
	public void run(){
		String str;
		while(true){
			str = next();
			count = 0;
			if(str.equals("#")){
				break;
			}
			
			StringBuilder bstr = new StringBuilder(str);
//			TreeSet<String> ans = solve(bstr);
			solve(str);

			TreeSet<String> tmp = new TreeSet<String>();
			for(String s : stList)tmp.add(s);
			for(String s : endList)tmp.add(s);

			int size = count;
			
			out.println(size);
			if(size > 10){
				for(String s : stList)out.println(s);
				for(String s : endList)out.println(s);
			}else{
				for(String s : tmp)out.println(s);
			}
		}
		
	}
	
	/*TreeSet<String>*/ void solve(String str){
		stList = new TreeSet<String>();
		endList = new TreeSet<String>();
		
		for(int i=0;i<(1<<str.length());i++){
			StringBuilder tmpStr = new StringBuilder(str);
			int tmp = i;
//			out.println(tmp);
			for(int j=0;j<str.length();j++){
				if((tmp&1)==1){
					decrypt(j, tmpStr);
				}
				tmp >>= 1;
			}
//			out.println(tmpStr);
			StringBuilder tmpStr2 = new StringBuilder().append(tmpStr);
			tmpStr = encrypt(tmpStr);
//			out.printf("%s %s\n", tmpStr.toString(), str.toString());
			if(tmpStr.toString().equals(str.toString())){
				count++;
				String ss = tmpStr2.toString();
				
				stList.add(ss);
				if(stList.size() > 5)stList.pollLast();
				
				endList.add(ss);
				if(endList.size() > 5)endList.pollFirst();
			}
			
		}
	}
	
	StringBuilder decrypt(int i, StringBuilder str){
		char c = str.charAt(i);
//		if(c == 'z')return str;
		str.setCharAt(i, (char)(c+1));
		return str;
	}
	
	StringBuilder encrypt(StringBuilder str){
		int len = str.length();
		for(char c='b';c<='z';c++){
			for(int i=0;i<len;i++){
				if(str.charAt(i)==c){
					str.setCharAt(i, (char)(c-1));
					break;
				}
			}
		}
		return str;
	}

	private static PrintWriter out;
	public static void main(String[] args){
		out = new PrintWriter(System.out);
		new Main().run();
		out.flush();
	}

	public Main(){
	}

	int ni(){
		int num = 0;
		String str = next();
		boolean minus = false;
		int i = 0;
		if(str.charAt(0) == '-'){
			minus = true;
			i++;
		}
		int len = str.length();
		for(;i < len; i++){
			char c = str.charAt(i);
			if(!('0' <= c && c <= '9')) throw new RuntimeException();
			num = num * 10 + (c - '0');
		}
		return minus ? -num : num;	}

	long nl(){
		long num = 0;
		String str = next();
		boolean minus = false;
		int i = 0;
		if(str.charAt(0) == '-'){
			minus = true;
			i++;
		}
		int len = str.length();
		for(;i < len; i++){
			char c = str.charAt(i);
			if(!('0' <= c && c <= '9')) throw new RuntimeException();
			num = num * 10l + (c - '0');
		}
		return minus ? -num : num;
	}

	String next(){
		int c;
		while(!isAlNum(c = read()));
		StringBuilder build = new StringBuilder();
		build.append((char)c);
		while(isAlNum(c = read())){
			build.append((char)c);
		}
		return build.toString();
	}
	private static byte[] inputBuffer = new byte[1024];
	private static int bufferLength = 0;
	private static int bufferIndex = 0;
	
	private static int read(){
		if(bufferLength < 0) throw new RuntimeException();
		if(bufferIndex >= bufferLength){
			try{
				bufferLength = System.in.read(inputBuffer);
				bufferIndex = 0;
			}catch(IOException e){
				throw new RuntimeException(e);
			}
			if(bufferLength <= 0) return (bufferLength = -1);
		}
		return inputBuffer[bufferIndex++];
	}
	
	private static boolean isAlNum(int c){
		return '!' <= c && c <= '~';
	}
	void debug(Object... os){
		out.println(Arrays.deepToString(os));
	}
}
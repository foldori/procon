import java.io.*;
import java.util.*;

class Main{
	long[] x;
	long n;
	long m;

	public void run(){
		n = nl();
		m = nl();
		x = new long[(int)m];
		for(int i=0;i<m;i++){
			x[i] = nl();
		}

		if(m == n){
			out.println(0);
			return;
		}
		
		long l = 0;
		long r = n * 3 / 2;
		long t = -1;
		while(r - l > 1){
			t = (l + r) / 2;
			// out.println(l);
			// out.println(r);
			// out.println(t);
			
			if(canCheckAll(t)){
				r = t;
			}else{
				l = t;
			}
		}
		out.println(r);
	}

	boolean canCheckAll(long t){

		long finished = 0;
		for(int i=0;i<m;i++){
			long l = x[i] - finished - 1;
			if(t < l){
				return false;
			}
			long r = Math.max(t-2*l, (t-l)/2);
			// out.print(finished + " ");
			if(r < t){
				finished = x[i] + r;
			}else{
				finished = x[i] + t;
			}
//			finished = x[i] + Math.min(r, t);
		}
		// out.println(finished);
		// out.println(finished >= n);
		return finished >= n;
	}

	private static PrintWriter out;
	public static void main(String[] args){
		out = new PrintWriter(System.out);
		new Main().run();
		out.flush();
	}

	public Main(){

	}

	int ni(){
		int num = 0;
		String str = next();
		boolean minus = false;
		int i = 0;
		if(str.charAt(0) == '-'){
			minus = true;
			i++;
		}
		int len = str.length();
		for(;i < len; i++){
			char c = str.charAt(i);
			if(!('0' <= c && c <= '9')) throw new RuntimeException();
			num = num * 10 + (c - '0');
		}
		return minus ? -num : num;	}

	long nl(){
		long num = 0;
		String str = next();
		boolean minus = false;
		int i = 0;
		if(str.charAt(0) == '-'){
			minus = true;
			i++;
		}
		int len = str.length();
		for(;i < len; i++){
			char c = str.charAt(i);
			if(!('0' <= c && c <= '9')) throw new RuntimeException();
			num = num * 10l + (c - '0');
		}
		return minus ? -num : num;
	}

	double nd(){
		return Double.parseDouble(next());
	}

	String next(){
		int c;
		while(!isAlNum(c = read()));
		StringBuilder build = new StringBuilder();
		build.append((char)c);
		while(isAlNum(c = read())){
			build.append((char)c);
		}
		return build.toString();
	}
	String nextLine(){
		int c;
		while(!isAlNumOrSpace(c = read()));
		StringBuilder build = new StringBuilder();
		build.append((char)c);
		while(isAlNumOrSpace(c = read())){
			build.append((char)c);
		}
		return build.toString();
	}
	private static byte[] inputBuffer = new byte[1024];
	private static int bufferLength = 0;
	private static int bufferIndex = 0;
	
	private static int read(){
		if(bufferLength < 0) throw new RuntimeException();
		if(bufferIndex >= bufferLength){
			try{
				bufferLength = System.in.read(inputBuffer);
				bufferIndex = 0;
			}catch(IOException e){
				throw new RuntimeException(e);
			}
			if(bufferLength <= 0) return (bufferLength = -1);
		}
		return inputBuffer[bufferIndex++];
	}
	
	private static boolean isAlNum(int c){
		return '!' <= c && c <= '~';
	}
	private static boolean isAlNumOrSpace(int c){
		return isAlNum(c) || c == ' ' || c == '\t';
	}
	void debug(Object... os){
		out.println(Arrays.deepToString(os));
	}
}